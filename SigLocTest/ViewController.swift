import UIKit
import CoreLocation
import UserNotifications

class ViewController: UIViewController {

  let locationManager = CLLocationManager()
  let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .short
    return formatter
  }()

  override func viewDidLoad() {
    super.viewDidLoad()
    configureLocationServices()
    configureNotifications()
  }
}

// MARK: CLLocationManagerDelegate

extension ViewController: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let location = locations.last else { return }
    let title = "Sig. Location Update"
    let date = dateFormatter.string(from: location.timestamp)
    let coords = "\(location.coordinate.longitude), \(location.coordinate.latitude)"
    displayNotification(title: title, subtitle: date, body: coords)
  }

  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    displayNotification(title: "Error", subtitle: "Location Manager", body: error.localizedDescription)
  }
}

// MARK: UNUserNotificationCenterDelegate

extension ViewController: UNUserNotificationCenterDelegate {
  func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    completionHandler([.sound, .alert, .badge])
  }
}

// MARK: Private

extension ViewController {
  fileprivate func configureLocationServices() {
    locationManager.delegate = self
    locationManager.allowsBackgroundLocationUpdates = true
    locationManager.pausesLocationUpdatesAutomatically = false
    // Request authorization, if needed.
    let authorizationStatus = CLLocationManager.authorizationStatus()
    switch authorizationStatus {
    case .notDetermined:
      // Request authorization.
      locationManager.requestAlwaysAuthorization()
      break
    default:
      break
    }
    locationManager.startMonitoringSignificantLocationChanges()

  }

  fileprivate func configureNotifications() {
    UNUserNotificationCenter.current().delegate = self
    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { _, _ in }
  }

  fileprivate func displayNotification(title: String, subtitle: String, body: String) {
    let content = UNMutableNotificationContent()
    content.title = title
    content.subtitle = subtitle
    content.body = body
    let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: nil)
    UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
  }
}

